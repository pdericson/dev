defmodule Dev do
  def clone(url) do
    uri =
      cond do
        String.match?(url, ~r/^git@/) ->
          URI.parse("ssh://" <> url)

        true ->
          URI.parse(url)
      end

    path = Path.join(["src", uri.host, Path.dirname(uri.path)])

    File.mkdir_p!(path)

    System.cmd("git", ["clone", url], cd: path)
  end
end
