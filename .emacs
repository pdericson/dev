(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(create-lockfiles nil)
 '(elpy-rpc-python-command "python3")
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(initial-scratch-message nil)
 '(markdown-fontify-code-blocks-natively t)
 '(package-selected-packages
   '(adoc-mode alchemist blacken dockerfile-mode elixir-mode elm-mode elpy erlang exec-path-from-shell flymake-go go-autocomplete go-eldoc go-mode groovy-mode haskell-mode magit markdown-mode protobuf-mode yaml-mode zenburn-theme))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(transient-mark-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(load-theme 'zenburn t)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(global-set-key (kbd "C-c c") 'compile)
(global-set-key (kbd "<f5>") 'compile)
(global-set-key (kbd "C-c r") 'recompile)
(global-set-key (kbd "<f7>") 'recompile)

(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max))))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;; elixir
(add-hook 'elixir-mode-hook
          (lambda () (add-hook 'before-save-hook 'elixir-format nil t)))

;; python
(add-hook 'python-mode-hook 'blacken-mode)

(elpy-enable)
;(use-package elpy
;  :ensure t
;  :defer t
;  :init
;  (advice-add 'python-mode :before 'elpy-enable))

;; golang
;(require 'go-autocomplete)

(defun go-mode-setup ()
 (go-eldoc-setup)
 (add-hook 'before-save-hook 'gofmt-before-save))
(add-hook 'go-mode-hook 'go-mode-setup)
